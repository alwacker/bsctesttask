//
//  File.swift
//  BSCTestTaskTests
//
//  Created by Oleksandr Vaker on 01/07/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation
import Combine
@testable import BSCTestTask

class ListModuleServiceMock: ListModuleService {
    let mockData = [Note(id: 1, title: "aaa"), Note(id: 2, title: "bbb")]
    
    init() {
        super.init(api: ListModuleApi(baseUrl: ""))
    }
    
    override func getNotes() -> AnyPublisher<[Note], Error> {
        return Just(mockData)
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }
}


