//
//  BSCTestTaskTests.swift
//  BSCTestTaskTests
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import XCTest
import Combine
@testable import BSCTestTask

class ListNotesViewModelTest: XCTestCase {

    var viewModel: ListNotesViewModel!
    var disposeBag = Set<AnyCancellable>()
    
    override func setUp() {
        self.viewModel = ListNotesViewModel(service: ListModuleServiceMock(), router: ListModuleRouterMock())
    }
    
    func testSections() {
        viewModel.didLoad.send()
        viewModel.sections.sink(receiveCompletion: { _ in } ) { (data) in
            XCTAssertEqual(data, [Note(id: 1, title: "aaa"), Note(id: 2, title: "bbb")])
        }.store(in: &disposeBag)
    }
    
    func test_AddButtonPressed() {
        viewModel.addButtonPressed.send(State.addNewNote)
        viewModel.sections.sink(receiveCompletion: { _ in } ) { (data) in
            XCTAssertEqual(data, [Note(id: 1, title: "aaa"), Note(id: 2, title: "bbb")])
        }.store(in: &disposeBag)
    }
    
    func test_selectedModelPressed() {
          viewModel.selectedModel.send(State.openDetail(Note(id: 2, title: "bbb")))
          viewModel.sections.sink(receiveCompletion: { _ in } ) { (data) in
              XCTAssertEqual(data, [Note(id: 1, title: "aaa"), Note(id: 2, title: "bbb")])
          }.store(in: &disposeBag)
      }
    
    func test_deleteModel() {
        viewModel.deleteModel.send(Note(id: 2, title: "bbb"))
        viewModel.sections.sink(receiveCompletion: { _ in } ) { (data) in
            XCTAssertEqual(data, [Note(id: 1, title: "aaa"), Note(id: 2, title: "bbb")])
        }.store(in: &disposeBag)
    }
}
