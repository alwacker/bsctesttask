//
//  ListModuleRouterMock.swift
//  BSCTestTaskTests
//
//  Created by Oleksandr Vaker on 01/07/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation
import Combine
@testable import BSCTestTask

class ListModuleRouterMock: ListModuleRouter {
    init() {
        let container = DIContainer(gateway: ApiGateway(baseUrl: ""))
        super.init(container: container, transitionHandler: AppRouter(container: container))
    }
    
    override func openDetail(with state: State) -> AnyPublisher<Void, Never> {
        return Just(()).eraseToAnyPublisher()
    }
}
