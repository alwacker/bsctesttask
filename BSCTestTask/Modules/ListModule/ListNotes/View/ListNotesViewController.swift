//
//  ListNotesViewController.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import UIKit
import Combine

class ListNotesViewController: BaseViewController {
    private struct Cells {
        static let listNoteCell = ReusableCell<ListNoteCell>(nibName: "ListNoteCell")
    }
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView()
            tableView.rowHeight = UITableView.automaticDimension
        }
    }
    
    private let viewModel: ListNotesViewModel
    private var disposeBag = Set<AnyCancellable>()

    init(viewModel: ListNotesViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "ListNotesViewController", bundle: nil)
        viewModel.didLoad.send()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "My notes"
        setupBinding()
        setupNavigationItem()
    }
    
    private func setupNavigationItem() {
        let addButton = UIButton(type: .contactAdd)
        addButton.publisher(for: .touchUpInside)
            .map { _ in State.addNewNote }
            .subscribe(viewModel.addButtonPressed)
            .store(in: &disposeBag)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: addButton)
    }
    
    private func setupBinding() {
        viewModel.sections
            .bind(subscriber: tableView.rowsSubscriber(
                reusableCell: Cells.listNoteCell,
                cellConfig: { (cell, indexPath, model) in
                    cell.setupUI(with: model)
                }, selectedModel: { [weak self] item in
                    self?.viewModel.selectedModel.send(State.openDetail(item))
                }, removedModel: { [weak self] item in
                    self?.viewModel.deleteModel.send(item)
                })
            ).store(in: &disposeBag)
    }
}
