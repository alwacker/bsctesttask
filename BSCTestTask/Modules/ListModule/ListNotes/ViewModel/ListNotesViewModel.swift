//
//  ListNotesViewModel.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation
import Combine

enum State: Equatable {
    case addNewNote
    case openDetail(Note)
    
    static func == (lhs: State, rhs: State) -> Bool {
        switch (lhs, rhs) {
        case (let .openDetail(l), let .openDetail(r)):
            return l.id == r.id && l.title == r.title
        default:
            return true
        }
    }
}

class ListNotesViewModel {
    //inputs
    let didLoad = PassthroughSubject<Void, Never>()
    let addButtonPressed = PassthroughSubject<State, Never>()
    let selectedModel = PassthroughSubject<State, Never>()
    let deleteModel = PassthroughSubject<Note, Never>()
    //outputs
    let sections = PassthroughSubject<[Note], Error>()

    private var disposeBag = Set<AnyCancellable>()

    init(service: ListModuleService, router: ListModuleRouter) {
        let loaded = didLoad
            .setFailureType(to: Error.self)
        
        let delete = deleteModel
            .map { $0.id }
            .setFailureType(to: Error.self)
            .flatMap(service.deleteNote)
        
        let merge = Publishers.Merge(addButtonPressed, selectedModel)
            .flatMap(router.openDetail(with:))
            .setFailureType(to: Error.self)
        
        Publishers.Merge3(loaded, merge, delete)
            .flatMap(service.getNotes)
            .subscribe(sections)
            .store(in: &disposeBag)
    }
}
