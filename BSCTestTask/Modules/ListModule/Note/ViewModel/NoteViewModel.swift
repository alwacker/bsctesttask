//
//  NoteViewModel.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 01/07/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation
import Combine

class NoteViewModel {
    //inputs
    let didLoad = PassthroughSubject<Void, Never>()
    let text = PassthroughSubject<String, Never>()
    let confirmButton = PassthroughSubject<Void, Never>()
    let dismissButton = PassthroughSubject<Void, Error>()
    
    //outputs
    let title: AnyPublisher<String?, Never>
    let textNote: AnyPublisher<String?, Never>
    
    private var disposeBag = Set<AnyCancellable>()
    
    init(service: ListModuleService, state: State, context: ListModuleRouter.Context) {
        let state = didLoad.map { _ in state }
        
        title = state.map {
            switch $0 {
            case .addNewNote:
                return "Write your note"
            case .openDetail:
                return "Edit your note"
            }
        }.eraseToAnyPublisher()
        
        textNote = state.map {
            switch $0 {
            case .openDetail(let note):
                return note.title
            default:
                return ""
            }
        }.eraseToAnyPublisher()
        
        let addNote = confirmButton
            .combineLatest(state)
            .filter { $0.1 == .addNewNote }
            .combineLatest(text)
            .map { $0.1 }
            .setFailureType(to: Error.self)
            .flatMap(service.createNote(note:))
        
        let editedNote = confirmButton
            .combineLatest(state)
            .map { state -> Note? in
                switch state.1 {
                case .openDetail(let note):
                    return note
                default:
                    return nil
                }
            }
            .unwrap()
            .combineLatest(text)
            .map { ($0.0.id, $0.1) }
            .setFailureType(to: Error.self)
            .flatMap(service.updateNote)
            
        Publishers.Merge3(addNote, editedNote, dismissButton)
            .subscribe(context.dismiss)
            .store(in: &disposeBag)
    }
}
