//
//  NoteViewController.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import UIKit
import Combine

class NoteViewController: BaseViewController {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var confirmButton: UIButton!
    @IBOutlet private weak var textField: UITextField!
    @IBOutlet private weak var dissmissButton: UIButton!
    @IBOutlet private weak var contentView: UIView! {
        didSet {
            contentView.layer.cornerRadius = 20
            contentView.layer.borderColor = UIColor.black.cgColor
            contentView.layer.borderWidth = 0.4
        }
    }
    
    private let viewModel: NoteViewModel
    private var disposeBag = Set<AnyCancellable>()
    
    init(viewModel: NoteViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "NoteViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        viewModel.didLoad.send()
    }
    
    private func setupBindings() {
        textField.publisher(for: .editingChanged)
            .map { [weak self] _ in self?.textField.text }
            .unwrap()
            .subscribe(viewModel.text)
            .store(in: &disposeBag)
        
        viewModel.title
            .assign(to: \.text, on: titleLabel)
            .store(in: &disposeBag)
        
        viewModel.textNote
            .assign(to: \.text, on: textField)
            .store(in: &disposeBag)
        
        confirmButton.publisher(for: .touchUpInside)
            .map { _ in () }
            .subscribe(viewModel.confirmButton)
            .store(in: &disposeBag)
        
        dissmissButton.publisher(for: .touchUpInside)
            .map { _ in () }
            .setFailureType(to: Error.self)
            .subscribe(viewModel.dismissButton)
            .store(in: &disposeBag)
    }
}
