//
//  ListModuleRouter.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation
import Combine

class ListModuleRouter {
    
    private let container: DIContainer
    private let transitionHandler: TransitionHandler
    private var disposeBag = Set<AnyCancellable>()
    
    init(container: DIContainer, transitionHandler: TransitionHandler) {
        self.container = container
        self.transitionHandler = transitionHandler
    }
    
    class Context {
        let dismiss = PassthroughSubject<Void, Error>()
    }
    
    func openDetail(with state: State) -> AnyPublisher<Void, Never> {
        let context = Context()
        let viewModel = NoteViewModel(service: container.services.listModuleService, state: state, context: context)
        let vc = NoteViewController(viewModel: viewModel)
        transitionHandler.modal(controller: vc, animated: true)
        return .create { [weak self] (subscriber) -> Cancellable in
            guard let self = self else { return AnyCancellable {} }
            context.dismiss.sink(receiveCompletion: { _ in }) {
                subscriber.send(())
                subscriber.send(completion: .finished)
            }.store(in: &self.disposeBag)
            return AnyCancellable {
                vc.dismiss(animated: true, completion: nil)
                self.disposeBag.forEach {
                    $0.cancel()
                }
            }
        }
    }
    
}
