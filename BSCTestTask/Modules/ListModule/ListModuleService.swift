//
//  ListModuleService.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation
import Combine

class ListModuleService {
    private let api: ListModuleApi
    
    init(api: ListModuleApi) {
        self.api = api
    }
    
    func getNotes() -> AnyPublisher<[Note], Error> {
        return api.getNotes()
    }
    
    func createNote(note: String) -> AnyPublisher<Void, Error> {
        return api.createNote(note: note)
    }
    
    func updateNote(id: Int, note: String) -> AnyPublisher<Void, Error> {
        return api.updateNote(id: id, note: note)
    }
    
    func deleteNote(id: Int) -> AnyPublisher<Void, Error> {
        return api.deleteNote(id: id)
    }
}
