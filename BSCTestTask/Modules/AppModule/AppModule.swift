//
//  AppModule.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation
private let baseURL = "http://private-9aad-note10.apiary-mock.com"

class AppModule {
    private let container: DIContainer
    
    init() {
         let apiGateway = ApiGateway(baseUrl: baseURL)
         container = DIContainer(gateway: apiGateway)
     }
    
    public lazy var router: AppRouter = {
        return .init(container: container)
    }()
    
}
