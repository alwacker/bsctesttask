//
//  AppRouter.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation
import UIKit

class AppRouter {
    private let container: DIContainer
    
    init(container: DIContainer) {
        self.container = container
    }
    
    public func showNotesList() -> UIViewController {
        let router = ListModuleRouter(container: container, transitionHandler: self)
        let vm = ListNotesViewModel(
            service: container.services.listModuleService,
            router: router)
        let vc = ListNotesViewController(viewModel: vm)
        let root = NavigationViewController(rootViewController: vc)
        return root
    }
}
