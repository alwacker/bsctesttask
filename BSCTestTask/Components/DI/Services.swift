//
//  Services.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation

public class Services {
    private let gateway: ApiGateway
    
    init(gateway: ApiGateway) {
        self.gateway = gateway
    }
    
    lazy var listModuleService: ListModuleService = {
        return .init(api: ListModuleApi(base: gateway))
    }()
}
