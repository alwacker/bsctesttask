//
//  DIContainer.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation

class DIContainer {
    let services: Services
    
    init(gateway: ApiGateway) {
        self.services = Services(gateway: gateway)
    }
}

