//
//  NotesEntity.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation

struct Note: Decodable, Hashable, Equatable {
    let id: Int
    let title: String
}
