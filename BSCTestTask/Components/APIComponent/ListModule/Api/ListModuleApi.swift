//
//  ListModuleApi.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation
import Combine

class ListModuleApi: BaseApi {
    func getNotes() -> AnyPublisher<[Note], Error> {
        return request(endPoint: .notes)
    }
    
    func createNote(note: String) -> AnyPublisher<Void, Error> {
        let params = ["title": note]
        let headers = ["Content-Type" : "application/json"]
        return request(endPoint: .notes, method: .post, params: params, headers: headers)
    }
    
    func updateNote(id: Int, note: String) -> AnyPublisher<Void, Error> {
        let params = ["title": note]
        let headers = ["Content-Type" : "application/json"]
        let endpoint = String(format: ApiEndPoint.noteId.rawValue, id)
        return request(endPoint: endpoint, method: .put, params: params, headers: headers)
    }
    
    func deleteNote(id: Int) -> AnyPublisher<Void, Error> {
        let endpoint = String(format: ApiEndPoint.noteId.rawValue, id)
        return request(endPoint: endpoint, method: .delete)
    }
}
