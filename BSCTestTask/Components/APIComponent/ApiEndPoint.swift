//
//  ApiEndPoint.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation

public enum ApiEndPoint: String {
    case notes = "/notes"
    case noteId = "/notes/%d"
}
