//
//  ApiProvider.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation
import Combine
import UIKit

public enum Method: String {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}

public class ApiProvider {
    public static let instance = ApiProvider()
    private let logQueue = DispatchQueue(label: "com.BSC.api-log", qos: .background)
    
    private lazy var session: URLSession = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 120
        configuration.waitsForConnectivity = true
        configuration.httpMaximumConnectionsPerHost = 5
        configuration.requestCachePolicy = .returnCacheDataElseLoad
        configuration.urlCache = .shared
        return URLSession(configuration: configuration)
    }()
    
    private func log(message: String) {
        logQueue.async {
            print(message)
        }
    }
    
    public func request<T: Decodable>(
        endPoint: String,
        method: Method = .get,
        params: Parameters? = nil,
        headers: HTTPHeaders? = nil,
        httpCodes: HTTPCodes = .success
    ) -> AnyPublisher<T, Error> {
        do {
            let request = try createRequest(
                endPoint,
                method: method,
                parameters: params,
                headers: headers)
            return session
                .dataTaskPublisher(for: request)
                .requestJSON(httpCodes: httpCodes)
        } catch let error {
            return Fail<T, Error>(error: error)
                .eraseToAnyPublisher()
        }
    }
    
    public func request(
           endPoint: String,
           method: Method = .get,
           params: Parameters? = nil,
           headers: HTTPHeaders? = nil,
           httpCodes: HTTPCodes = .success
       ) -> AnyPublisher<Void, Error> {
           do {
               let request = try createRequest(
                   endPoint,
                   method: method,
                   parameters: params,
                   headers: headers)
               return session
                .dataTaskPublisher(for: request)
                .tryMap { $0 }
                .receive(on: DispatchQueue.main)
                .map { _ in () }
                .eraseToAnyPublisher()
           } catch let error {
               return Fail<Void, Error>(error: error)
                   .eraseToAnyPublisher()
           }
       }
    
    func createRequest(
        _ url: String,
        method: Method = .get,
        parameters: Parameters? = nil,
        headers: HTTPHeaders? = nil
    ) throws -> URLRequest {
        let debugString = "DEBUG: - ApiProvider: create request to \(url), method \(method), params: \(String(describing: parameters))"
        self.log(message: debugString)
        guard let url = URL(string: url) else {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = headers
        if let params = parameters {
            request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        }
        return request
    }
}

private extension Publisher where Output == URLSession.DataTaskPublisher.Output {
    func requestJSON<T: Decodable>(httpCodes: HTTPCodes) -> AnyPublisher<T, Error> {
        return tryMap {
            guard let code = ($0.1 as? HTTPURLResponse)?.statusCode else {
                throw ApiError.unexpectedResponse
            }
            guard httpCodes.contains(code) else {
                throw ApiError.httpCode(code)
            }
            return $0.0
        }
        .decode(type: T.self, decoder: JSONDecoder())
        .receive(on: DispatchQueue.main)
        .eraseToAnyPublisher()
    }
}
