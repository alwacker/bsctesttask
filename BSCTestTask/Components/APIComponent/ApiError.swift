//
//  ApiError.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation

public enum ApiError: Error {
    case jsonParseError(message: String)
    case error(message: String)
    case encodingError(message: String)
    case invalidURL
    case httpCode(HTTPCode)
    case unexpectedResponse
    case imageProcessing([URLRequest])
}

extension ApiError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .invalidURL: return "Invalid URL"
        case let .httpCode(code): return "Unexpected HTTP code: \(code)"
        case .unexpectedResponse: return "Unexpected response from the server"
        case .imageProcessing: return "Unable to load image"
        case .jsonParseError(let message): return message
        case .error(let message): return message
        case .encodingError(let message): return message
        }
    }
}
