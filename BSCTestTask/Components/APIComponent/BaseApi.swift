//
//  BaseApi.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation
import Combine
import UIKit

public typealias HTTPHeaders = [String: String]
public typealias Parameters = [String: Any]

public typealias HTTPCode = Int
public typealias HTTPCodes = Range<HTTPCode>

extension HTTPCodes {
    public static let success = 200 ..< 300
}

public class BaseApi {
    private let base: ApiGateway
    
    public init(base: ApiGateway) {
        self.base = base
    }
    
    public init(baseUrl: String) {
        self.base = ApiGateway(baseUrl: baseUrl)
    }
    
    private func getEndpointUrl(endPoint: ApiEndPoint) -> String {
        return base.getEndpointURL(endPoint: endPoint)
    }
    
    private func getEndpointUrl(endPoint: String) -> String {
        return base.getEndpointURL(endPoint: endPoint)
    }
    
    public func request(endPoint: ApiEndPoint, method: Method = .get, params: [String: Any]? = nil, headers: HTTPHeaders? = nil) -> AnyPublisher<Void, Error> {
        return ApiProvider.instance.request(endPoint: getEndpointUrl(endPoint: endPoint), method: method, params: params, headers: headers)
    }
    
    public func request(endPoint: String, method: Method = .get, params: [String: Any]? = nil, headers: HTTPHeaders? = nil) -> AnyPublisher<Void, Error> {
          return ApiProvider.instance.request(endPoint: getEndpointUrl(endPoint: endPoint), method: method, params: params, headers: headers)
      }
    
    public func request<T:Decodable>(endPoint: ApiEndPoint, method: Method = .get, params: [String: Any]? = nil, headers: HTTPHeaders? = nil) -> AnyPublisher<T, Error> {
        return ApiProvider.instance.request(endPoint: getEndpointUrl(endPoint: endPoint), method: method, params: params, headers: headers)
    }
}
