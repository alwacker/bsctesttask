//
//  ApiGateway.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation

protocol Gateway {
    func getEndpointURL(endPoint: ApiEndPoint) -> String
    func getEndpointURL(endPoint: String) -> String
}

public class ApiGateway: Gateway {
    private let baseUrl: String
    
    public init(baseUrl: String) {
        self.baseUrl = baseUrl
    }
    
    public func getEndpointURL(endPoint: ApiEndPoint) -> String {
        let url = endPoint.rawValue.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? endPoint.rawValue
        return prepareUrl(endPoint: url)
    }
    
    public func getEndpointURL(endPoint: String) -> String {
        return prepareUrl(endPoint: endPoint)
    }
    
    private func prepareUrl(endPoint: String) -> String {
        let url = endPoint.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? endPoint
        return "\(baseUrl)\(url)"
    }
}
