//
//  Navigator.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 01/07/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation
import UIKit

class Navigator {
    static var rootViewController: UIViewController? {
        if let window = UIApplication.shared.delegate?.window {
            return window?.rootViewController
        }
        return nil
    }
    
    static func getRootNavigationViewController(rootController: UIViewController) -> UINavigationController {
        let rootViewController = Navigator.rootViewController
        if let rootNavigationController = rootViewController as? NavigationViewController {
            return rootNavigationController
        } else {
            let navigationController = NavigationViewController(rootViewController: rootController)
            navigationController.navigationBar.isTranslucent = false
            navigationController.navigationBar.isOpaque = true
            return navigationController
        }
    }
    
    static func showModalViewController(
        _ vc: UIViewController,
        inNavigationController: Bool,
        animated: Bool) {
        let root = Navigator.rootViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.view.backgroundColor = .init(white: 0.4, alpha: 0.8)
        root?.present(vc, animated: animated)
    }

}
