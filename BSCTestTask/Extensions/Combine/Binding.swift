//
//  Binding.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation
import Combine

public typealias Binding = Subscriber

public extension Publisher where Failure == Error {
    func bind<B: Binding>(subscriber: B) -> AnyCancellable
        where B.Failure == Error, B.Input == Output {
            handleEvents(receiveSubscription: { subscription in
                subscriber.receive(subscription: subscription)
            }).sink(receiveCompletion: { error in
                _ = subscriber.receive(completion: error)
            }) { value in
                _ = subscriber.receive(value)
            }
    }
}
