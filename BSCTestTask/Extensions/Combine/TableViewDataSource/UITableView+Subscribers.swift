//
//  UITableView+Subscribers.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation
import UIKit
import Combine

extension UITableView {
    /// A table view specific `Subscriber` that receives `[Element]` input and updates a single section table view.
    /// - Parameter cellIdentifier: The Cell ID to use for dequeueing table cells.
    /// - Parameter cellType: The required cell type for table rows.
    /// - Parameter cellConfig: A closure that receives an initialized cell and a collection element
    ///     and configures the cell for displaying in its containing table view.
    public func rowsSubscriber<CellType, Items>(
        reusableCell: ReusableCell<CellType>,
        cellConfig: @escaping TableViewItemsController<[Items]>.CellConfig<Items.Element, CellType>,
        selectedModel: @escaping TableViewItemsController<[Items]>.SelectedModel,
        removedModel: @escaping TableViewItemsController<[Items]>.RemovedModel)
        -> AnySubscriber<Items, Error> where CellType: UITableViewCell,
        Items: RandomAccessCollection,
        Items: Equatable {
            return rowsSubscriber(.init(reusableCell: reusableCell, cellConfig: cellConfig, selectedModel: selectedModel, removedModel: removedModel))
    }
    
    /// A table view specific `Subscriber` that receives `[Element]` input and updates a single section table view.
    /// - Parameter source: A configured `TableViewItemsController<Items>` instance.
    public func rowsSubscriber<Items>(_ source: TableViewItemsController<[Items]>)
        -> AnySubscriber<Items, Error> where
        Items: RandomAccessCollection,
        Items: Equatable {
            
            source.tableView = self
            dataSource = source
            delegate = source
            
            return AnySubscriber<Items, Error>(receiveSubscription: { subscription in
                subscription.request(.unlimited)
            }, receiveValue: { [weak self] items -> Subscribers.Demand in
                guard let self = self else { return .none }
                
                if self.dataSource == nil {
                    self.dataSource = source
                }
                
                source.updateCollection([items])
                return .unlimited
            }) { _ in }
    }
}

