//
//  UITableView+ReusableKit.swift
//  BSCTestTask
//
//  Created by Oleksandr Vaker on 30/06/2020.
//  Copyright © 2020 Oleksandr Vaker. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell: CellType {
}

extension UITableView {
    
    // MARK: Cell
    
    /// Registers a generic cell for use in creating new table cells.
    public func register<Cell>(_ cell: ReusableCell<Cell>) {
        if let nib = cell.nib {
            self.register(nib, forCellReuseIdentifier: cell.identifier)
        } else {
            self.register(Cell.self, forCellReuseIdentifier: cell.identifier)
        }
    }
    
    /// Returns a generic reusable cell located by its identifier.
    public func dequeue<Cell>(_ cell: ReusableCell<Cell>) -> Cell? {
        return self.dequeueReusableCell(withIdentifier: cell.identifier) as? Cell
    }
    
    /// Returns a generic reusable cell located by its identifier.
    public func dequeue<Cell>(_ cell: ReusableCell<Cell>, for indexPath: IndexPath) -> Cell {
        return self.dequeueReusableCell(withIdentifier: cell.identifier, for: indexPath) as! Cell
    }
}
